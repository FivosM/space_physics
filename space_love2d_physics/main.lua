
local planetObject = require("Modules/planetObject")
local playerObject = require("Modules/playerObject")

objectPool = {}

love.physics.setMeter(64)
world = love.physics.newWorld(0, 0, true)

function love.load()

    table.insert(objectPool, planetObject.new(100, 200, 15, 200))
    table.insert(objectPool, planetObject.new(300, 450, 15, 350))
    table.insert(objectPool, planetObject.new(500, 690, 15, 250))
    table.insert(objectPool, planetObject.new(320, 200, 15, 100))
    table.insert(objectPool, playerObject.new(300, 300, 0, 0))
end

function love.update(dt)
    world:update(dt)
    for i=1, #objectPool do
        objectPool[i]:update(objectPool)
    end
end

function love.draw()
    for i=1, #objectPool do
        objectPool[i]:draw()
    end
end