local planet = {}

local function load()

end

local function update()

end

local function draw( self )
    love.graphics.circle( "fill", self.x, self.y, self.radius )
end

function planet.new( planetX, planetY, radius, mass )
    planetPhysics = {}
    planetPhysics.body = love.physics.newBody(world, playerX, playerY)
    planetPhysics.shape =  love.physics.newCircleShape(10)
    planetPhysics.fixture = love.physics.newFixture(planetPhysics.body, planetPhysics.shape)
    return {
        physicsBody = planetPhysics,
        load = load,
        update = update,
        draw = draw,
        x = planetX or 0,
        y = planetY or 0,
        radius = radius or 10,
        mass = mass or 2
    }
end

return planet
