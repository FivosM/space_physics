local player = {}

local function calculateGravity( self, planetList )
        for i=1, #planetList do
            if planetList[i] ~= self then
                local distanceX = planetList[i].x - self.x
                local distanceY = planetList[i].y - self.y
                local distance = math.abs(distanceX) + math.abs(distanceY)
                if distance ~= 0 then
                    local gravitationalForce = self.gravitationalConstant * (planetList[i].mass * self.mass) / math.sqrt(distance)
                        if gravitationalForce > self.maxGravitationalForce then
                            gravitationalForce = self.maxGravitationalForce
                        end
                    self.speedX = self.speedX + gravitationalForce * (distanceX / distance) * self.timeSpeed
                    self.speedY = self.speedY + gravitationalForce * (distanceY / distance) * self.timeSpeed
                end
            end
        end
end

--self.speedX = self.speedX + gravitationalForce * (distanceX / distance) * dt * self.timeSpeed
--self.speedY = self.speedY + gravitationalForce * (distanceY / distance) * dt * self.timeSpeed

local function handleInput( self )
    local inputX, inputY = 0, 0
    if love.keyboard.isDown(self.inputUp) then
        inputY = inputY - self.inputSpeed
    end
    if love.keyboard.isDown(self.inputDown) then
        inputY = inputY + self.inputSpeed
    end
    if love.keyboard.isDown(self.inputRight) then
        inputX = inputX + self.inputSpeed
    end
    if love.keyboard.isDown(self.inputLeft) then
        inputX = inputX - self.inputSpeed
    end
    return inputX, inputY
end

local function update( self, planetList )
    dt = love.timer.getDelta()
    calculateGravity(self, planetList)
    local inputX, inputY = handleInput(self)
    self.x = self.x + (self.speedX + inputX) * dt
    self.y = self.y + (self.speedY + inputY) * dt
end

local function draw( self )
    love.graphics.circle( "fill", self.x, self.y, 10 )
end

function player.new( playerX, playerY, initialX, initialY )
    return {
        draw = draw,
        update = update,
        x = playerX,
        y = playerY,
        mass = 1,
        timeSpeed = 0.1, 
        gravitationalConstant = 1,
        maxGravitationalForce = 999,
        inputSpeed = 250,
        speedX = initialX or 0,
        speedY = initialY or 0,
        inputUp = "w",
        inputDown = "s",
        inputRight = "d",
        inputLeft = "a"
    }
end

return player
