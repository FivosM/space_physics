
local planetObject = require("Modules/planetObject")
local playerObject = require("Modules/playerObject")

objectPool = {}

function love.load()

    table.insert(objectPool, planetObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), love.math.random( 15, 30 ), love.math.random( 150, 400 )))
    table.insert(objectPool, planetObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), love.math.random( 15, 30 ), love.math.random( 150, 400 )))
    table.insert(objectPool, planetObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), love.math.random( 15, 30 ), love.math.random( 150, 400 )))
    table.insert(objectPool, planetObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), love.math.random( 15, 30 ), love.math.random( 150, 400 )))
    table.insert(objectPool, playerObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), 0, 0))
    table.insert(objectPool, playerObject.new(love.math.random( 100, 700 ), love.math.random( 100, 500 ), 0, 0))
end

function love.update(dt)
    for i=1, #objectPool do
        objectPool[i]:update(objectPool)
    end
end

function love.draw()
    for i=1, #objectPool do
        objectPool[i]:draw()
    end
end